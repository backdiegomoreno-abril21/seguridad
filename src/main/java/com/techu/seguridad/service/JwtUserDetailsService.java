package com.techu.seguridad.service;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        if ("techu".equals(s)) {
            return new User("techu", "$2y$12$5w4Ck/ppcHJm5XBjcC.gT.rZHUsiQ7687nY3FMRDZ/I5Q5PjEiaUy", new ArrayList<>());
        }
        throw new UsernameNotFoundException(String.format("Usuario %s no encontrado", s));
    }
}
